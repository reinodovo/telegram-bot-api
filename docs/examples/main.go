package main

import (
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	api "gitlab.com/reinodovo/telegram-bot-api"
)

var bot api.TelegramBotAPI
var botId int64

func main() {
	botId, _ = strconv.ParseInt(os.Getenv("BOT_USER_ID"), 10, 64)

	chatId, _ := strconv.ParseInt(os.Getenv("CHAT_ID"), 10, 64)

	var err error
	bot, err = api.NewTelegramBot(api.TelegramConfiguration{
		Token:   os.Getenv("BOT_TOKEN"),
		Timeout: 60,
		ChatID:  chatId,
	}, api.RabbitMQStreamConfiguration{
		Uri:        os.Getenv("RABBITMQ_URI"),
		StreamName: os.Getenv("RABBITMQ_STREAM_NAME"),
	})
	if err != nil {
		log.Panic(err)
	}
	err = bot.SetGroupUpdatesHandler(handleUpdates)
	if err != nil {
		log.Panic(err)
	}
	err = bot.SetBotUpdatesHandler(handleUpdates)
	if err != nil {
		log.Panic(err)
	}

	quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM)
	<-quitChannel

	log.Printf("closing telegram bot...")
}

func handleUpdates(updates tgbotapi.UpdatesChannel) {
	for update := range updates {
		log.Printf("update: %v\n", update)
		if update.Message == nil {
			continue
		}
		if update.Message.From.ID == botId {
			// ignore messages from itself to avoid infinite loops
			continue
		}
		bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "hi!"))
	}
}
