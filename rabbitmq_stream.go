package api

import (
	"encoding/json"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/rabbitmq/rabbitmq-stream-go-client/pkg/amqp"
	"github.com/rabbitmq/rabbitmq-stream-go-client/pkg/stream"
)

type RabbitMQStreamConfiguration struct {
	Uri        string
	StreamName string
}

type rabbitMQTelegramMessageStream struct {
	env      *stream.Environment
	producer *stream.Producer
	consumer *stream.Consumer
	channel  chan tgbotapi.Update
	config   RabbitMQStreamConfiguration
}

func newRabbitMQTelegramMessageStream(streamConfig RabbitMQStreamConfiguration) (*rabbitMQTelegramMessageStream, error) {
	messageStream := &rabbitMQTelegramMessageStream{
		channel: make(chan tgbotapi.Update),
		config:  streamConfig,
	}
	var err error
	messageStream.env, err = stream.NewEnvironment(
		stream.NewEnvironmentOptions().SetUri(streamConfig.Uri))
	if err != nil {
		return nil, err
	}
	err = messageStream.connectProducer()
	if err != nil {
		return nil, err
	}
	err = messageStream.connectConsumer()
	if err != nil {
		return nil, err
	}
	return messageStream, nil
}

func (messageStream *rabbitMQTelegramMessageStream) connectProducer() error {
	var err error
	messageStream.producer, err = messageStream.env.NewProducer(messageStream.config.StreamName, nil)
	if err != nil {
		return err
	}
	go reconnectOnClose(messageStream.producer.NotifyClose(), messageStream.connectProducer)
	return nil
}

func (messageStream *rabbitMQTelegramMessageStream) connectConsumer() error {
	var err error
	messageStream.consumer, err = messageStream.env.NewConsumer(messageStream.config.StreamName, messageStream.handleNewMessages, nil)
	if err != nil {
		return err
	}
	go reconnectOnClose(messageStream.consumer.NotifyClose(), messageStream.connectConsumer)
	return nil
}

func reconnectOnClose(channelClose stream.ChannelClose, reconnect func() error) {
	<-channelClose
	reconnect()
}

func (messageStream *rabbitMQTelegramMessageStream) handleNewMessages(consumerContext stream.ConsumerContext, message *amqp.Message) {
	for _, data := range message.Data {
		var telegramMessage tgbotapi.Message
		err := json.Unmarshal(data, &telegramMessage)
		if err != nil {
			continue
		}
		messageStream.channel <- tgbotapi.Update{Message: &telegramMessage}
	}
}

func (messageStream *rabbitMQTelegramMessageStream) send(telegramMessage tgbotapi.Message) error {
	content, err := json.Marshal(telegramMessage)
	if err != nil {
		return err
	}
	message := amqp.NewMessage(content)
	err = messageStream.producer.Send(message)
	return err
}

func (messageStream *rabbitMQTelegramMessageStream) updatesChannel() (tgbotapi.UpdatesChannel, error) {
	return messageStream.channel, nil
}

func (messageStream *rabbitMQTelegramMessageStream) close() error {
	return messageStream.env.Close()
}
