module gitlab.com/reinodovo/telegram-bot-api

go 1.18

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/rabbitmq/rabbitmq-stream-go-client v1.0.0-rc9
)

require (
	github.com/frankban/quicktest v1.14.3 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/klauspost/compress v1.14.2 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
