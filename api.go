package api

import (
	"errors"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type UpdateHandler = func(updates tgbotapi.UpdatesChannel)

type TelegramConfiguration struct {
	Token   string
	ChatID  int64
	Timeout int
}

type TelegramBot struct {
	API            *tgbotapi.BotAPI
	stream         *rabbitMQTelegramMessageStream
	telegramConfig TelegramConfiguration
	streamConfig   RabbitMQStreamConfiguration
}

type TelegramBotAPI interface {
	SetGroupUpdatesHandler(UpdateHandler) error
	SetBotUpdatesHandler(UpdateHandler) error
	Send(tgbotapi.Chattable) (tgbotapi.Message, error)
	SendToGroupOnly(tgbotapi.Chattable) (tgbotapi.Message, error)
}

func NewTelegramBot(telegramConfig TelegramConfiguration, streamConfig RabbitMQStreamConfiguration) (*TelegramBot, error) {
	var err error
	bot := &TelegramBot{
		telegramConfig: telegramConfig,
		streamConfig:   streamConfig,
	}
	bot.API, err = tgbotapi.NewBotAPI(bot.telegramConfig.Token)
	if err != nil {
		return nil, err
	}
	if streamConfig.Uri == "" {
		bot.stream = nil
		return bot, nil
	}
	bot.stream, err = newRabbitMQTelegramMessageStream(bot.streamConfig)
	if err != nil {
		return nil, err
	}
	return bot, nil
}

func (bot *TelegramBot) SetGroupUpdatesHandler(groupUpdatesHandler UpdateHandler) error {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = bot.telegramConfig.Timeout
	groupUpdates := bot.API.GetUpdatesChan(u)
	go groupUpdatesHandler(groupUpdates)
	return nil
}

func (bot *TelegramBot) SetBotUpdatesHandler(botUpdatesHandler UpdateHandler) error {
	if bot.stream == nil {
		return errors.New("stream is not configured")
	}
	botUpdates, err := bot.stream.updatesChannel()
	if err != nil {
		return err
	}
	go botUpdatesHandler(botUpdates)
	return nil
}

func (bot *TelegramBot) SendToGroupOnly(message tgbotapi.Chattable) (tgbotapi.Message, error) {
	return bot.API.Send(message)
}

func (bot *TelegramBot) Send(message tgbotapi.Chattable) (tgbotapi.Message, error) {
	sentMessage, err := bot.SendToGroupOnly(message)
	if err != nil {
		return sentMessage, err
	}
	if bot.stream == nil {
		return sentMessage, nil
	}
	if sentMessage.Chat.ID != bot.telegramConfig.ChatID {
		return sentMessage, nil
	}
	return sentMessage, bot.stream.send(sentMessage)
}
